export const contentType = (_, res, next) => {
  res.setHeader("Content-Type", "application/json");
  next();
};
