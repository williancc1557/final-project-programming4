import { logger } from "../../utils/logger.js";

export const loggerMiddleware = (req, _, next) => {
  logger.debug(`Router executed (${req.method}) ${req.url}`);
  next();
};
