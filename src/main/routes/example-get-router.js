import { adaptRoute } from "../adapters/express-route-adapter.js";
import { makeExampleGetController } from "../factories/example-get.js";

export const exampleGetRouter = (router) => {
  router.get("/", adaptRoute(makeExampleGetController()));
};
