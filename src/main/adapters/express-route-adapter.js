export const adaptRoute = (controller) => {
  return async (req, res) => {
    const httpRequest = {
      body: req.body,
      header: req.headers,
      params: req.params,
    };

    const httpResponse = await controller.handle(httpRequest);

    res.status(httpResponse.statusCode).json(httpResponse.body);
  };
};
