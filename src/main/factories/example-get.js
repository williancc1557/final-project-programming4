import { ExampleGetController } from "../../presentation/controllers/example-get.js";

export const makeExampleGetController = () => {
  return new ExampleGetController();
};
