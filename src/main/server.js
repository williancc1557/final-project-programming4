import { logger } from "../utils/logger.js";
import env from "./config/env.js";

const bootstrap = async () => {
  const app = (await import("./config/app.js")).default;

  app.listen(env.port, () =>
    logger.info(`Server started with http://localhost:${env.port}`)
  );
};

bootstrap().then(() => {
  logger.info("API made by (Capybara Coders)");
});
