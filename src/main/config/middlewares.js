import { bodyParser } from "../middlewares/body-parse.js";
import { contentType } from "../middlewares/content-type.js";
import { cors } from "../middlewares/cors.js";
import { loggerMiddleware } from "../middlewares/logger.js";

export const setupMiddlewares = (app) => {
  app.use(bodyParser);
  app.use(contentType);
  app.use(cors);
  app.use(loggerMiddleware);
};
