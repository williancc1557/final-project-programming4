import { Router } from "express";
import { exampleGetRouter } from "../routes/example-get-router.js";

export const setupRoutes = (app) => {
  const router = Router();

  app.use(router);

  exampleGetRouter(router);
};
