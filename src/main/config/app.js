import express from "express";
import { setupMiddlewares } from "./middlewares.js";
import { setupRoutes } from "./router.js";

const app = express();

setupMiddlewares(app);
setupRoutes(app);

export default app;
