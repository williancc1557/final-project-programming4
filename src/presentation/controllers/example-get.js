import { ok } from "../helpers/http-helper.js";

export class ExampleGetController {
  async handle(httpRequest) {
    return ok({
      message: "Hello, world!",
    });
  }
}
