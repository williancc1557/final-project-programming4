import { ServerError } from "../errors/server-error.js";
import { UnauthorizedError } from "../errors/unauthorized-error.js";

export const badRequest = (error) => ({
  statusCode: 400,
  body: { error: error.message },
});

export const serverError = () => ({
  statusCode: 500,
  body: { error: new ServerError().message },
});

export const ok = (data) => ({
  statusCode: 200,
  body: data,
});

export const conflict = (error) => ({
  statusCode: 409,
  body: { error: error.message },
});

export const unauthorized = () => ({
  body: { error: new UnauthorizedError().message },
  statusCode: 401,
});
