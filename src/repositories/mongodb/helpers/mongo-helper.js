import { MongoClient } from "mongodb";

export const mongoHelper = {
  client: null,
  async connect(url) {
    this.client = await new MongoClient(url || process.env.MONGO_URL).connect();
  },

  async disconnect() {
    await this.client.close();
    this.client = null;
  },

  async getCollection(name) {
    try {
      return this.client.db().collection(name);
    } catch {
      await this.connect();
      return this.client.db().collection(name);
    }
  },
};
